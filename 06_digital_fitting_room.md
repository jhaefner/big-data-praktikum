# Digital Fitting Room

## Motivation

In previous work an application for product matching has been developed. This application, called [Smart Mirror](https://ceur-ws.org/Vol-3075/paper10.pdf), analyses the user's worn clothes using computer vision and deep learning approaches. For this purpose, the clothing segments in the photo are recognized by means of object recognition and converted into embedding vectors in order to compare them with a product database for product matching.
Research and development in the field of Generative Adversarial Networks [(GANs)](https://ieeexplore.ieee.org/abstract/document/8253599) enable algorithms to manipulate images or generate new ones. These methods are used, for example, in [deep fakes](http://noiselab.ucsd.edu/ECE228_2018/Reports/Report16.pdf), but can also be used for a digital fitting of clothes to ensure a new type of [digital interaction](https://arxiv.org/abs/2206.14180).

## Goal
In the course of this scientific work, image-based methods for digital fitting of clothing are to be investigated and implemented. This will also include evaluating the implemented approach on selected benchmark [datasets](https://www.dropbox.com/s/10bfat0kg4si1bu/zalando-hd-resized.zip?dl=0).

## Work packages

### 1 Concept

Become familiar with Generative Adversarial Networks in combination with pose estimation and human parsing models, based on the shared approaches to digital cloth fitting.
Create a first sketch for the architecture of your solution.

### 2 Implementation

Implement your application. Your code needs to be documented and tested to ensure future usability and correctness. At the end of this phase you should also run your system on benchmark datasets to see how well the implemented approach performs.

### 3 Presentation

The presentation (10 min + 5 min discussion) should showcase how your system works (architecture and basic ideas), as well as present the results.

### Literature & Links

- [DeepFashion2](https://openaccess.thecvf.com/content_CVPR_2019/papers/Ge_DeepFashion2_A_Versatile_Benchmark_for_Detection_Pose_Estimation_Segmentation_and_CVPR_2019_paper.pdf)
- [SmartMirror](https://ceur-ws.org/Vol-3075/paper10.pdf)
- [Implementation SmartMirror](https://nbn-resolving.org/urn:nbn:de:bsz:l189-qucosa2-833147)
- [GAN](https://ieeexplore.ieee.org/abstract/document/8253599)
- [HR-VITON](https://arxiv.org/abs/2206.14180)
- [HR-VITON-GIT](https://github.com/sangyun884/HR-VITON)
- [VITON_HD](https://arxiv.org/abs/2103.16874)
- [VITON-HD-GIT](https://github.com/shadow2496/VITON-HD)
- [DataSet](https://www.dropbox.com/s/10bfat0kg4si1bu/zalando-hd-resized.zip?dl=0)
