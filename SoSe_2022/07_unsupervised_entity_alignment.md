# Simple but effective unsupervised entity alignment 

## Motivation

Integrating data from different sources is necessary to answer complex information needs. Finding nodes in knowledge graphs, which refer to the same real-world entities (Entity Resolution) is a necessary step in this data integration process. A recent trend in research has been the use of knowledge graph embeddings, which encodes entities as lower-dimensional vectors in an embedding space, where similar entities are close to each other. Such systems usually consist of two main components:
  
  1. Initialize the entities with their features by utilizing pre-trained (sub-)word embeddings. Aggregate these embeddings to create a unified representation for all entities.
  2. Exploit the relations between entities to adapt the initialized entity embeddings to better fit the training data. This is usually done via graph convolutional networks or tensor-factorization.

A [recent publication](https://arxiv.org/pdf/2109.02363v2.pdf) showed, that for simple cross-lingual entity alignment settings one can cast the alignment problem as [assignment problem](https://en.wikipedia.org/wiki/Assignment_problem). This enables the use of simpler and faster solutions.

## Goal
Your task in this practicum is to reimplement [this](https://github.com/MaoXinn/SEU) proof-of-concept in order to enable benchmarking this solution on [more general datasets](https://forayer.readthedocs.io/en/latest/source/apidoc.html#datasets). This will also include investigating a variety of [aggregation strategies](https://github.com/PrincetonML/SIF) to generate the initial entity embeddings.


## Work packages

### 1 Concept 

Make yourself familiar with general concepts of [knowledge graph embeddings](https://www.youtube.com/watch?v=gX_KHaU8ChI) and [entity alignment](http://www.vldb.org/pvldb/vol13/p2326-sun.pdf).
Create an initial sketch for the architecture of your solution. Keep in mind that this system needs to be modular enough to encompass different (sub-) word embedding aggreation strategies.

### 2 Implementation

Implement your application. Your code needs to be documented and tested to ensure future usability and correctness. At the end of this phase you should also run your system on the benchmark datasets to see how well this approach works on this task.

### 3 Presentation

The presentation (10 min + 5 min discussion) should showcase how your system works (architecture and basic ideas), as well as present the results. 

### Literature & Links

All the links are already mentioned above but here they are all in one place: 

  - [The central publication of the approach you will implement](https://arxiv.org/pdf/2109.02363v2.pdf)
  - [the corresponding code](https://github.com/MaoXinn/SEU)
  - [Knowledge Graph Embeddings Tutorial](https://www.youtube.com/watch?v=gX_KHaU8ChI)
  - [Entity Alignment Survey](http://www.vldb.org/pvldb/vol13/p2326-sun.pdf)
