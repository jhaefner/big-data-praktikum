# Stabilität der Record-Linkage-Qualität für Datensatzvariationen

## Motivation
Ein wichtiger Bestandteil von Datenintegrationprozessen mit mehreren Datenquellen ist die Erkennung von Datensätzen, die zur gleichen Real-Welt-Entität, z.B. einer Person, gehören. Bei diesem Record Linkage werden Datensätze i.d.R. paarweise auf Basis ihrer Attribute wie Name, Geburtsdatum und Adresse verglichen und als Match bzw. Non-Match klassifiziert.
Die Linkage Qualität hängt dabei von der geeigneten Festlegung diverser Parameter ab, z.B. der Gewichtung der Attribute. Geeignete Parameterwerte können beispielsweise mithilfe eines Testdatensatzes bestimmt werden.
Dessen Eigenschaften weichen jedoch ggf. von denen im tatsächlichen Anwendungsfall ab, was zu einer verschlechterten Linkage Qualität führen kann.

## Zielstellung
Ziel des Praktikums ist es, die Linkage Qualität für verschiedene Varianten eines Datensatzes zu untersuchen. Hierfür wird ein Testdatensatz bereitgestellt, der Personen aller Altersgruppen enthält. Für diesen soll zunächst ein Record Linkage durchgeführt und dabei die Parameter optimiert werden.
Aus dem Gesamtdatensatz sind Teildatensätze anhand von Selektionskriterien abzuleiten, z.B. entsprechend von Altersspanne und Geschlecht.
Auf diesen Teildatensätzen soll ein Record Linkage mit den global optimierten Parametern durchgeführt werden. Die Ergebnisse sollen auf ihre Stabilität untersucht werden.

## Arbeitspakete

### 1. Einarbeitung und Lösungsskizze
Zunächst soll in einem Konzept skizziert werden, wie und mit welchen Werkzeugen die Datensatzvariation, das Record Linkage, das Experiment-Tracking und die Auswertung umgesetzt werden können. Es sind Metriken festzulegen, anhand derer die Stabilität der Qualität bewertet wird.

### 2. Implementierung und Evaluation
Basierend auf der Lösungsskizze ist der experimentelle Aufbau umzusetzen und die Stabilität der Linkage Qualität zu evaluieren.

### 3. Vortrag
Ausarbeitung einer 10-minütigen Präsentation, in welcher die Aufgabenstellung, die Lösungsskizze und die Ergebnisse dargestellt werden.

## Links

* [Python Record Linkage Toolkit](https://recordlinkage.readthedocs.io/en/latest/index.html)
* [mlflow (Experiement tracking)](https://mlflow.org/docs/latest/tracking.html#)
