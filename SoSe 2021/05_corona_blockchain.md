# Schnellere Übertragung von Corona-Fallzahlen mittels privater Blockchain

## Motivation
Auf das Corona-Virus Getestete warten nach einem Testabstrich tagelang auf ihr Ergebniss. Gesundheitsämter kommen beim Aufnehmen und Weiterleiten von Infektionsmeldungen nicht hinterher. Inzidenzwerte müssen durch verspätete Übermittlungen nachkorregiert werden, was Prognosen erschweren kann. Die Übermittlung von Testergebnissen ist ein Prozess mit vielen Beteiligten mit unterschiedlichen Wünschen und (Datenschutz-)Ansprüchen. Eine datensouveräne digitale Lösung muss her.

## Zielstellung
Ziel dieses Themas ist es eine DLT-basierte Anwendung zur Übermittlung von Testergebnissen und Berechnung des R-Wertes mithilfe von Hyperledger Fabric zu erstellen. 

Folgende Use-Cases sind ein erster Anhaltspunkt. Recherchieren sie gern selbst welche Teilprozesse es noch beim Corona-Testen gibt und welche Anforderungen diese mit sich bringen. Welche davon dann implementiert werden müssen legen wir in fest, sobald Sie recherchiert haben und sich mit Fabric vertraut gemacht haben.
* __Ein Gesundheitsamt berechtigt ein Labor Testergebnisse in den Fabric-Channel des Landkreises zu schreiben__
* __Ein Labor schreibt ein Testergebniss in die Blockchain__
* __Getestete rufen ihre Ergebnisse aus der Blockchain ab, z.B. über einen Code und das Geburtsdatum__
* __Ein Landesamt aggregiert die Fallzahlen aus den Channels mehrerer Kreis-Ämter und berechnet den aktuellen R-Wert__
* __Per Skript laden sie einen großen Datensatz historischer Fallzahlen aus mehreren Landkreisen in ihre Anwendung, um den Durchsatz zu testen__

## Arbeitspakete

__1 - Anwendungsdesign und Lösungsskizze__

Zunächst müssen sie sich mit den Konzepten und Funktionen von Hyperledgeer Fabric vertraut machen. Machen sie sich kurz damit vertraut wie Testergebnisse im Moment übertragen werden und welche Daten dabei genau erfasst werden. Dann analysieren Sie wie die gesammelten Use-Cases mit den Funktionen von Fabric umgesetzt werden können. Ihre Ergebnisse fassen Sie in einer Lösungsskizze zusammen

__2 - Implementierung und Evaluation__

Basierend auf der Lösungsskizze implementieren Sie die Anwendung prototpyisch. D.h. insbesondere, dass Sie Smart Contracts schreiben und CLI-Programme oder Skripte die diese Aufrufen. Ggf schreiben Sie auch ein Skript um ein Fabric-Netzwerk zu erstellen und dynamisch um Nodes zu erweitern.

__3 - Vortrag__

In einem 10-minütigen Präsentation sollen sie die Aufgabenstellung vorstellen und anhand ausgewählter Use-Cases und deren Probleme aufzeigen wie sie diese mit Fabric gelöst haben.

## Literatur
- [Tutorial für Hyperledger Fabric](https://hyperledger-fabric.readthedocs.io/en/release-2.2/tutorial/commercial_paper.html)
