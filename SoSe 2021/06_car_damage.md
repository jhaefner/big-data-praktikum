# Klassifikation von Unfallschäden bei PKWs mittels Deep Learning (in Kooperation mit Adesso)

## Motivation

Die Bearbeitung von Versicherungsfällen bei PKWs ist oft ein langwieriger Prozess der viele Instanzen durchlaufen muss. Um diesen Prozess zu verkürzen, wäre es hilfreich eine erste Einschätzung des Schadens automatisiert durchzuführen. Anschließend können schnell spezialisierte Gutachter beauftragt werden oder Hinweise auf potentiellen Versicherungsbetrug frühzeitig erkannt und näher untersucht werden.

Für diese Aufgaben werden bisher häufig regelbasierte Systeme genutzt, die nur einen kleinen Teil der Schadensfälle korrekt einordnen können und zudem schwierig zu erweitern sind. Neuere Methoden im Bereich der Bildklassifikation verwenden dagegen häufig neuronale Netze, da so vielfältige Probleme gelöst und bei Bedarf schnell erweitert werden können. 

## Zielstellung

Ziel dieses Themas ist die Entwicklung einer Anwendung, die in der Lage ist Bilddaten von Fahrzeugen anhand der Position und Schwere des Schadens zu klassifizieren. Hierbei sind gängige Frameworks wie PyTorch, Tensorflow/Keras oder ähnliches zu verwenden. 

Diese Aufgabe lässt sich in 3 Abschnitte unterteilen:

* __Aufarbeitung und Erweiterung des Datensatzes__
* __Klassifikation der Schwere des Schadens__
* __Klassifikation der Position des Schadens__

Als Datengrundlage wird ein Datensatz mit Bilddaten von Schadensfällen und unbeschädigten Fahrzeugen zur Verfügung gestellt. Dieser muss angepasst und bei Bedarf erweitert werden. 

Um die Qualität der Klassifikatoren zu bewerten, können in Anlehnung an Kaggle-Challenges klassifizierte Validierungsdaten während des Praktikums an die Betreuer geschickt werden.

## Arbeitspakete

__1 - Anwendungsdesign und Lösungsskizze__

Zur Lösung der Aufgabe ist es zunächst notwendig sich mit dem bestehenden Datensatz vertraut zu machen und Probleme zu identifizieren. Zudem soll nach weiteren Datenquellen gesucht werden. Darüber hinaus müssen sich die Studierenden über gängige Frameworks informieren und sich in eines einarbeiten. Dabei sollte auch das Thema Transfer-Learning beachtet werden.

Anschließend ist eine Lösungsskizze anzufertigen, die beschreibt, was die konkreten Anforderungen der zu entwickelnden Software sind und welche Schritte dafür durchgeführt werden müssen. 

__2 - Implementierung und Evaluation__

Basierend auf der Lösungsskize sind nach der Aufbereitung der Daten die notwendigen neuronalen Netze zu implementieren. Diese sind zunächst mithilfe der vorhandenen Daten zu evaluieren und iterativ zu verbessern. Hierfür können auch weitere Datenquellen hinzugezogen werden.

Die Anwendung ist in Python 3 mithilfe eines selbst gewählten Deep Learning Frameworks zu implementieren. Für das Training der Modelle kann ein Zugang zu entsprechender Hardware ermöglicht werden.

Die Evaluation erfolgt anschließend mithilfe eines separaten Validierungsdatensatzes. Dabei wird die Genauigkeit der Klassifikatoren überprüft und die Fähigkeit das Problem auch für unbekannte Daten zu generalisieren.

__3 - Vortrag__

Ausarbeitung einer 10-minütigen Präsentation, in welcher die Aufgabenstellung, die Lösungsskizze 
(kurze Beschreibung der Architektur und des Datensatzes) und die Evaluationsergebnisse vorgestellt werden. 


## Literatur & Links
- [Keras](https://keras.io/)
- [PyTorch](https://pytorch.org/)
- [Image Classification and Transfer Learning with Keras](https://blog.keras.io/building-powerful-image-classification-models-using-very-little-data.html)
- [Adesso SE](https://www.adesso.de/) 
