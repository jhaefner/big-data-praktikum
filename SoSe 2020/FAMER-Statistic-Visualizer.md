# Visualisierung von FAMER Statistiken

## Motivation
[FAMER](https://dbs.uni-leipzig.de/research/projects/object_matching/famer) ist ein skalierbares Entity Resolution Framework, das auf Apache Flink basiert.
Es ermöglicht die Integration von vielen großen Datenquellen mithilfe diverser Blocking, Linking und Clusteringimplementationen. 
Das ermitteln von identischen Entitäten in mehreren Datenquellen würde bei naiver Herangehensweise (alle Entitäten miteinander vergleichen) nicht handhabbar sein.
Deswegen wird u.a. Blocking benutzt um die Komplexität zu senken und unnötige Vergleiche auszuschließen.
Beispielsweise könnte man bei Personen die Vergleiche auf Entitäten einschränken, die mit demselben Nachnamen beginnen.
Diese Entitäten werden einem Block zugeordnet und nur Entitäten innerhalb eines Blocks werden miteinander verglichen.
Beim Vergleichen werden Ähnlichkeitsmaße benutzt (z.B. Editierdistanz) und aggregiert um anschließend einen Ähnlichkeitsgraphen zu erstellen.
Schlußendlich wird dieser Ähnlichkeitsgraph genutzt um Entitäten zu clustern.


Um bei diesem Vorgang gute Ergebnisse zu erzielen ist es notwendig sich eine Übersicht über die Datenlage zu verschaffen.

## Zielstellung
Das Thema zielt auf die Extraktion von Statistiken aus [EPGM-Graphen](https://github.com/dbs-leipzig/gradoop) und den einzelnen Entity Resolution (ER) Schritten. Diese sollen dann visualisiert werden (bspw. in einem Jupyter Notebook).

## Arbeitspakete
### 1. Ermittelung von relevanten Statistiken
Zunächst gilt es herauszufinden, welche Statistiken relevant für den ER-Prozess sind. Hierbei können auch schon erste Überlegungen angestellt werden wie die Implementierung aussehen könnte um diese Statistiken zu erlangen.

### 2. Implementierung
Die ausgearbeiteten Statistiken sollen implementiert werden, sodass aus geladenen EPGM-Graphen die Werte berechnet und in einem sinnvollen Format gespeichert werden können. 

### 3. Visualisierung
Anschließend sollen diese berechneten Statistiken aussagekräftig visualisiert werden, beispielsweise in einem Jupyter Notebook.

### 4. Vortrag
Ausarbeitung einer 10-minütigen Präsentation, in welcher die Auswahl der relevanten Statistiken begründet wird und die Visualisierungen für einige Datensätze präsentiert wird.
