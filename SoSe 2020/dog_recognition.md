# Erkennung von Hundebellen

## Motivation

Die automatisierte Erkennung von Tierlauten auf Basis von Audiodaten ist ein aufstrebendes Forschungsfeld mit Anwendungen z.B. in der Biodiversitätsforschung, Klimastudien oder der Verhaltensforschung.
Zu diesem Zweck werden über längere Zeiträume Audiodaten in bestimmten Lebensräumen gesammelt und ausgewertet.
Die dabei anfallenden großen Datenmengen lassen sich nur mithilfe maschineller Lernverfahren verarbeiten, da eine manuelle Untersuchung wenig effizient und daher nicht sinnvoll durchführbar ist.

Ein Beispiel ist die automatisierte Erfassung der Art und Häufigkeit verschiedener Amphibien in einer Umgebung. Diese sind ein guter Indikator für die Biodiversität und 
geben damit Aufschluss über die ökologische Gesundheit des untersuchten Lebensraums.
Auch können mit solchen Untersuchungen Verhaltensstudien durchgeführt werden. 
Beispielweise lässt sich durch die automatisierte Detektion der Migrationszeiten bestimmter Vogelarten ein möglicher Einfluss klimatischer Veränderungen untersuchen.
 
Ein weiterer Teilaspekt dieses Forschungsgebiets ist die Klassifikation von Hundebellen. 
Bisherige Studien untersuchen die Frage, ob sich das Bellen von Hunden situationsspezifisch unterscheiden lässt und damit, ob das Bellen eine kommunikative Funktion hat.
Weitere Aufgabenstellungen können sein, verschiedene Hunderassen voneinander zu unterscheiden oder auch generelles Hundebellen zu detektieren. 

[//]: # (Es ist außerdem klar, dass eine erfolgreiche Klassifikation von der Qualität der zugrundeliegenden Audioaufnahmen abhängt, die durch Störgeräusche, Überlappungen von Signalen oder einen schlechten Signal-Rausch-Abstand beeinflusst sein kann. 
Eine entsprechende Signalverarbeitung kann daher notwendig sein.)

## Zielstellung

Die Aufgabe besteht darin, eine Anwendung zu erstellen, die mithilfe eines maschinellen Lernverfahrens allgemeines Hundebellen in Audiodaten erkennt, also eine binäre Einteilung in "Hund" und "kein Hund" vornimmt. 
Die Erkennung soll sich dabei nicht auf eine bestimmte Hunderasse beschränken. Es ist daher ein entsprechender Datensatz notwendig, der eine gewisse Vielfalt von Hunderassen beinhaltet. 
Für die Klassifikation soll ein geeignetes Machine-Learning-Verfahren ausgewählt und die besten Modell-Parameter für das Problem gefunden werden.

Hierbei ist von Interesse, welche Auswirkungen verschiedene Ansätze, Parameter sowie Trainings- und Test-Daten auf die Qualität der Vorhersage haben.


## Arbeitspakete

### 1. Datenextraktion
Zunächst muss ein geeigneter Datensatz gefunden werden, der eine entsprechende Vielfalt an Hunderassen enthält sowie eine Auswahl an Geräuschen, von denen das Bellen abgegrenzt werden soll, wie z.B.
Stimmen, Musik, andere Tiere etc. 
[AudioSet](https://research.google.com/audioset/dataset/index.html) bietet bspw. eine umfangreiche Sammlung manuell annotierter Audioevents aus Videodateien. 
Hier können entsprechende Datensätze durch Einschränkung auf eine geeignete Ontologieebene extrahiert werden (z.B. Animal>Domestic animals, pets>Dog>Bark).

### 2. Modellwahl und Implementierung
Durch Literatur-Recherche soll zunächst ein für das Problem geeignetes Machine-Learning-Modell ausgewählt und eine passende ML-Bibliothek gefunden werden, 
in welcher der entsprechende Algorithmus auf Basis der Daten trainiert und dessen Klassifikationsgenauigkeit evaluiert werden soll. 
Hierbei ist eine sinnvolle Unterteilung der Daten in Trainings- und Testdaten vorzunehmen.

[//]: # (Um die besten Parameter für das Modell zu finden, können diese z.B. durch Grid-Search ermittelt werden.)

### 3. Evaluierung
Das umgesetzte Verfahren ist hinsichtlich der Vorhersagequalität zu beurteilen. 
Hierzu eignen sich die Metriken Recall, Precision und F-Measure. 
Es ist zu untersuchen, inwieweit die Aufteilung von Trainings- und Testdaten, deren Umfang sowie verschiedene Parameter die Vorhersagequalität beeinflussen.

### 4. Vortrag
Ausarbeit einer 10-minütigen Präsentation, in welcher die Aufgabenstellung, die Lösungsskizze (kurze Beschreibung des Algorithmus, Nennung der verwendeten Bibliotheken) und die Ergebnisse der Evaluierung dargestellt werden. 


## Literatur

- [Automated classification of bird and amphibian calls using machine learning: A comparison of methods](https://www.researchgate.net/publication/222905186_Automated_classification_of_bird_and_amphibian_calls_using_machine_learning_A_comparison_of_methods)
- [Eavesdropping on the Arctic: Automated bioacoustics reveal dynamics in songbird breeding phenology](https://www.researchgate.net/publication/325887988_Eavesdropping_on_the_Arctic_Automated_bioacoustics_reveal_dynamics_in_songbird_breeding_phenology)
- [Classification of dog barks: A machine learning approach](https://www.researchgate.net/publication/5654967_Classification_of_dog_barks_A_machine_learning_approach)
- [Towards a robust analysis and classification of dog barking](https://dbs.uni-leipzig.de/de/publication/title/towards_a_robust_analysis_and_classification_of_dog_barking)
- [WhatFrog: A Comparison of Classification Algorithms for Automated Anuran Recognition](https://www.researchgate.net/publication/265855309_Automatic_Recognition_of_Anuran_Species_Based_on_Syllable_Identification)
- [Deep neural network approach to frog species recognition](https://www.researchgate.net/publication/320367088_Deep_neural_network_approach_to_frog_species_recognition)

## Links

- [tensorflow-sound-classification](https://www.iotforall.com/tensorflow-sound-classification-machine-learning-applications/)
- [YouTube-8M Tensorflow Starter Code](https://github.com/google/youtube-8m#youtube-8m-tensorflow-starter-code)