# Predict tracks that could belong to a playlist with Knowledge Graph Embeddings

## Motivation

Link prediction aims to predict relations that likely are missing in a knowledge graph. A variety of models exist nowadays that encode entities and relations of a knowledge graph in a low-dimensional vector space in order to predict missing links.

The structure of information from the music domain is well fitted to be represented as a knowledge graph, with artists, albums, tracks and playlists as nodes and a variety of relationship types connecting them.

[Several](https://zenodo.org/record/5002584) [datasets](https://www.aicrowd.com/challenges/spotify-million-playlist-dataset-challenge) exist, where spotify playlist data was shared, with the goal of performing music recommendation.

## Goal

Your task is to transform this data into a knowledge graph in order to use the [pykeen](https://github.com/pykeen/pykeen) framework for link prediction.

## Work packages

### 1. Transformation

Transform the dataset into a knowledge graph with a pipeline suitable for this amount of data.

### 2. Loading, Training and Evaluating

Create a [dataset class](https://pykeen.readthedocs.io/en/stable/extending/datasets.html) in order to load the data into pykeen and [train](https://pykeen.readthedocs.io/en/stable/tutorial/first_steps.html#training-a-model) and evaluate knowledge graph embedding models.

### 3. Presentation

Present your results in 10-minute presentation.

## Further reading

- [KGE evaluation of pykeen](https://arxiv.org/abs/2006.13365v5)
