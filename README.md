# Big-Data Praktikum

## Übersicht


Das Praktikum beinhaltet den Entwurf und die Realisierung einer Anwendung oder eines Algorithmus im Big-Data und Machine-Learning-Umfeld. In der Regel erfolgt die Implementierung unter Verwendung von Big-Data oder Deep-Learning-Frameworks. 

<!--break-->

Im einzelnen sind folgende Teilaufgaben zu lösen:

1. <b>Konzeptioneller Entwurf</b>. Es ist ein Entwurfsdokument anzufertigen, welches konzeptionell den Ablauf und die Architektur ihrer Anwendung darstellt. Im Dokument muss ersichtlich werden, welches Ziel verfolgt wird und welche Aufgaben dafür zu bewältigen sind. Das Dokument soll sich vom Umfang auf ca. 4 Seiten beschränken.
2. <b>Implementierung</b>. Basierend auf ihrem Entwurf soll die Anwendung realisiert werden. Das Resultat dieser Phase ist ein dokumentiertes, ausführbares Programm.
3. <b>Abschlusspräsentation</b> Am Ende des Praktikums stellt jede Gruppe ihr Projekt vor, wobei sie ihre Anwendung beschreibt sowie die Resultate präsentiert. Die Dauer der Präsentation soll ca. <b>10 Minuten</b> (+ 5 min. Diskussion) betragen.


## Anmeldung und Präsenzveranstaltungen


* Die Anmeldung zum Praktikum erfolgt über <a href="https://almaweb.uni-leipzig.de/" target = "_blank">Almaweb</a>. 
  * Bei Fragen und Problemen zur Anmeldung wenden Sie sich bitte immer an das Studienbüro via einschreibung(at)math.uni-leipzig.de

* Für die endgültige Anmeldung und Themenzuordnung melden Sie sich im Moodle unter folgender Adresse an:
<a href='https://moodle2.uni-leipzig.de/course/view.php?id=31409'> Anmeldung Moodle</a><br>
Sie haben bis zum 18.04. Zeit Ihre präferierten Themen und Wunschpartner anzugeben.

* <b>Vorbesprechung</b>
  * Erste Mailkommunikation für die Konkretisierung des Themas und die ersten Schritte werden mit dem Betreuer per Mail abgehalten.
  


## Testate


Das Praktikum gliedert sich in drei Teile. Nach jeder der drei Teilaufgaben wird eine Dokumentation oder ein Testat per Skype/Slack/ oder ähnliches durchgeführt. 
Die Art der Durchführung soll mit dem Betreuer individuell abgesprochen werden.
Zum erfolgreichen Absolvieren des Praktikums müssen <b>alle</b> drei Testate erfolgreich abgelegt werden. 
Wird ein Termin nicht eingehalten, verfallen die bereits erbrachten Teilleistungen. 
Die konkreten Termine für die ersten zwei Testate bzw. Abgabe sind mit dem Betreuer per E-Mail zu vereinbaren. 
Es gelten die nachfolgenden Fristen:

* Testat 1 (Entwurf): Ende Mai
* Testat 2 (Realisierung): Mitte-Ende Juli
* Testat 3 (Präsentation): tba


## Themen

<strong class="aktuell">Bitte vereinbaren Sie zeitnah einen Termin mit Ihrem Betreuer zur Besprechung der ersten Schritte!</strong>


| Nr | Thema | Betreuer | Technologie| Studenten | Material |
| -- |-------|----------|------------|-----------|----------|
| 01 | [Beispielthema mit Link zur Beschreibung](01_beispielthema.md) | Max Musterbetreuer | Python |  | [Paper]() |
| 02 | [Synthetische Zeitreihen](02_synthdata.md) | Burghardt | Python |  | -- |
| 03 | [Analyse und Vorhersage städtischer Emissionen](03_city_emissions.md) | Lange | Python |  | [Datensatz-Paper](https://www.nature.com/articles/sdata2018280) |
| 04 | [LID-DS to Beagle](04_LID-DS_to_Beagle.md) | Grimmer | Python |  | -- |
  | 05 | [Playlist Link Prediction](05_playlist_link_prediction.md) | Obraczka | Python |  | -- |
| 06 | [Digital Fitting Room](06_digital_fitting_room.md) | Häfner | Python |  | -- |
| 07 | []() |  | Python |  | -- |
| 08 | []() |  | Python |  | -- |
| 09 | []() |  | Python |  | -- |
| 10 | []() |  | Python |  | -- |
| 11 | []() |  | Python |  | -- |
| 12 | []() |  | Python |  | -- |
| 13 | []() |  | Python |  | -- |
| 14 | []() |  | Python |  | -- |



Teilnehmerkreis


Master-Studiengänge Data-Science und bei freien Plätzen auch Master-Studiengänge Informatik.  Die Teilnahme erfolgt in 2er-Gruppen, 
<strong class="aktuell">die Teilnehmerzahl ist auf ca. 24 Studierende beschränkt!</strong>. 
 Zu beachten ist, dass Studenten, die das Big-Data-Praktikum noch nicht belegt haben, bevorzugt werden. 

## Erwartete Vorkenntnisse

* **Java**/**Python**/...-Kenntnisse (siehe Technologie in Tabelle)
* Vorlesung Cloud Data Management und NoSQL-Datenbanken hilfreich
* Linux-Kenntnisse von Vorteil
* Git-Kenntnisse von Vorteil
